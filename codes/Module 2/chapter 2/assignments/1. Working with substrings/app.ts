var inp : HTMLInputElement = <HTMLInputElement>document.getElementById("inp");
var output : HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("output");

// As required, it assumes you are giving TYPESCRIPT as input.

function results() {
 if(inp.value != "TYPESCRIPT") {
  alert("Please pass \"TYPESCRIPT\" AS INPUT!!!");
 } else {
  output.innerHTML = inp.value.substr(4,11) + "<br>";
  output.innerHTML += inp.value.indexOf('E').toString();
 }
}