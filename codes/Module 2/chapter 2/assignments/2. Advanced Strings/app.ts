var input : HTMLInputElement = <HTMLInputElement>document.getElementById("input");
var output : HTMLParagraphElement = <HTMLParagraphElement> document.getElementById("otpt");

var splitString : string[];


function strFunc() {
 // Note, this will not actually change the value stored in input
 // Also we don't want any particular output so not checking the condition if "Virtual Labs Bootathon 2020" is provided.
 output.innerHTML = input.value.toUpperCase() + "<br>";
 output.innerHTML += input.value.toLowerCase() + "<br>";
 // Splitting the string.
 splitString = input.value.split(" ");
  //Loop to iterate through the list.
 for(var i = 0; i < splitString.length; i ++) {
  output.innerHTML += splitString[i] + "<br>";
 } 
}