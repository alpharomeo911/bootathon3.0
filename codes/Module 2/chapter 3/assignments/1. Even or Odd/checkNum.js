var input = document.getElementById("numJ");
var output = document.getElementById("otpt");
function checkNum() {
    // Converting to float.
    var num1 = parseInt(input.value);
    // parseInt is used because, i tried console logging eg 4.454 % 2 and precision was not exact.
    if (num1 % 2 == 0) {
        output.innerHTML = "<strong>EVEN</strong>";
    }
    else {
        output.innerHTML = "<strong>ODD</strong>";
    }
}
//# sourceMappingURL=checkNum.js.map