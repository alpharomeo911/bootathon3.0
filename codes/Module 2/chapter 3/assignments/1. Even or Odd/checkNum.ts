var input : HTMLInputElement = <HTMLInputElement> document.getElementById("numJ");
var output : HTMLParagraphElement = <HTMLParagraphElement> document.getElementById("otpt");

function checkNum() {
 // Converting to float.
 var num1 : number = parseInt(input.value);
// parseInt is used because, i tried console logging eg 4.454 % 2 and precision was not exact.
 
 if(num1 % 2 == 0) {
  output.innerHTML = "<strong>EVEN</strong>";
 } else {
  output.innerHTML = "<strong>ODD</strong>";
 }
}