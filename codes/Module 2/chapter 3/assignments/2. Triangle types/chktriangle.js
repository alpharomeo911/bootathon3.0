var side1 = document.getElementById("side1");
var side2 = document.getElementById("side2");
var side3 = document.getElementById("side3");
var output = document.getElementById("output");
function checkTriangle() {
    var trinagleA = +side1.value;
    var trinagleB = +side2.value;
    var trinagleC = +side3.value;
    if ((trinagleA == trinagleB) && (trinagleB == trinagleC)) {
        output.innerHTML = "Equilateral Triangle";
    }
    else if ((trinagleA == trinagleB) || (trinagleB == trinagleC)) {
        output.innerHTML = "Isosceles Triangle";
        // Checking if right angled
        if ((Math.pow(trinagleA, 2) == (Math.pow(trinagleB, 2) + Math.pow(trinagleC, 2))) || (Math.pow(trinagleB, 2) == (Math.pow(trinagleA, 2) + Math.pow(trinagleC, 2))) || (Math.pow(trinagleC, 2) == (Math.pow(trinagleB, 2) + Math.pow(trinagleA, 2)))) {
            output.innerHTML += "<br>Right-Angled Triangle";
        }
    }
    else {
        output.innerHTML = "Scalen Triangle";
    }
    // Checking if right angled
    if ((Math.pow(trinagleA, 2) == (Math.pow(trinagleB, 2) + Math.pow(trinagleC, 2))) || (Math.pow(trinagleB, 2) == (Math.pow(trinagleA, 2) + Math.pow(trinagleC, 2))) || (Math.pow(trinagleC, 2) == (Math.pow(trinagleB, 2) + Math.pow(trinagleA, 2)))) {
        output.innerHTML += "<br>Right-Angled Triangle";
    }
}
//# sourceMappingURL=chktriangle.js.map