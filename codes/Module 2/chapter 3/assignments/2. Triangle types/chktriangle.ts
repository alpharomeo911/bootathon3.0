var side1 : HTMLInputElement = <HTMLInputElement> document.getElementById("side1");
var side2 : HTMLInputElement = <HTMLInputElement> document.getElementById("side2");
var side3 : HTMLInputElement = <HTMLInputElement> document.getElementById("side3");
var output : HTMLParagraphElement = <HTMLParagraphElement> document.getElementById("output");

function checkTriangle() {
 var trinagleA : number = +side1.value;
 var trinagleB : number = +side2.value;
 var trinagleC : number = +side3.value;

 if((trinagleA == trinagleB) && (trinagleB == trinagleC)) {
  output.innerHTML = "Equilateral Triangle";
 } else if((trinagleA == trinagleB) || (trinagleB == trinagleC)) {
  output.innerHTML = "Isosceles Triangle"; 
  // Checking if right angled
  if((Math.pow(trinagleA, 2) == (Math.pow(trinagleB, 2) + Math.pow(trinagleC, 2))) || (Math.pow(trinagleB, 2) == (Math.pow(trinagleA, 2) + Math.pow(trinagleC, 2))) || (Math.pow(trinagleC, 2) == (Math.pow(trinagleB, 2) + Math.pow(trinagleA, 2)))) {
   output.innerHTML += "<br>Right-Angled Triangle";
  }
 } else {
  output.innerHTML = "Scalen Triangle";
 }
 // Checking if right angled
 if((Math.pow(trinagleA, 2) == (Math.pow(trinagleB, 2) + Math.pow(trinagleC, 2))) || (Math.pow(trinagleB, 2) == (Math.pow(trinagleA, 2) + Math.pow(trinagleC, 2))) || (Math.pow(trinagleC, 2) == (Math.pow(trinagleB, 2) + Math.pow(trinagleA, 2)))) {
  output.innerHTML += "<br>Right-Angled Triangle";
 }
}