var a1 = document.getElementById("x1");
var a2 = document.getElementById("x2");
var a3 = document.getElementById("x3");
var b1 = document.getElementById("y1");
var b2 = document.getElementById("y2");
var b3 = document.getElementById("y3");
var xx = document.getElementById("px");
var xy = document.getElementById("py");
// Function to calculate area.
function areaOfTriangle(x1, y1, x2, y2, x3, y3) {
    return Math.abs(((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2));
}
// Function to check
function checkFunction() {
    var x1 = +a1.value;
    var x2 = +a2.value;
    var x3 = +a3.value;
    var y1 = +b1.value;
    var y2 = +b2.value;
    var y3 = +b3.value;
    var px = +xx.value;
    var py = +xy.value;
    // Area ABC
    var ABC = areaOfTriangle(x1, y1, x2, y2, x3, y3);
    console.log(ABC);
    // Area of rest of the triangle.
    var PAB = areaOfTriangle(x1, y1, x2, y2, px, py);
    var PBC = areaOfTriangle(px, py, x2, y2, x3, y3);
    var PAC = areaOfTriangle(x1, y1, px, py, x3, y3);
    console.log(PBC);
    console.log(PBC);
    console.log(PAC);
    // Sum of areas
    var sumArea = PAC + PAB + PBC;
    if ((Math.abs((ABC) - sumArea)) < 0.000001) {
        document.getElementById("output").innerHTML = "The point lies inside the Triangle";
    }
    else {
        document.getElementById("output").innerHTML = "The point does not lie inside the Triangle";
    }
}
//# sourceMappingURL=app.js.map