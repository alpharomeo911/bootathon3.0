var a1 : HTMLInputElement = <HTMLInputElement> document.getElementById("x1");
var a2 : HTMLInputElement = <HTMLInputElement> document.getElementById("x2");
var a3 : HTMLInputElement = <HTMLInputElement> document.getElementById("x3");
var b1 : HTMLInputElement = <HTMLInputElement> document.getElementById("y1");
var b2 : HTMLInputElement = <HTMLInputElement> document.getElementById("y2");
var b3 : HTMLInputElement = <HTMLInputElement> document.getElementById("y3");
var xx : HTMLInputElement = <HTMLInputElement> document.getElementById("px");
var xy : HTMLInputElement = <HTMLInputElement> document.getElementById("py");

// Function to calculate area.
function areaOfTriangle(x1, y1, x2, y2, x3, y3) : number {
 return Math.abs(((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2)); 
}

// Function to check
function checkFunction() {
 var x1 : number = +a1.value;
 var x2 : number = +a2.value;
 var x3 : number = +a3.value;
 var y1 : number = +b1.value;
 var y2 : number = +b2.value;
 var y3 : number = +b3.value;
 var px : number = +xx.value;
 var py : number = +xy.value;

// Area ABC
var ABC : number = areaOfTriangle(x1, y1, x2, y2, x3, y3);
console.log(ABC);
// Area of rest of the triangle.
var PAB : number = areaOfTriangle(x1, y1, x2, y2, px, py);
var PBC : number = areaOfTriangle(px, py, x2, y2, x3, y3);
var PAC : number = areaOfTriangle(x1, y1, px, py, x3, y3);
console.log(PBC);
console.log(PBC);
console.log(PAC);
// Sum of areas
var sumArea : number = PAC + PAB + PBC;

if((Math.abs((ABC) - sumArea)) < 0.000001) {
 document.getElementById("output").innerHTML = "The point lies inside the Triangle";
} else {
 document.getElementById("output").innerHTML = "The point does not lie inside the Triangle";
}

}