var expression: HTMLInputElement = <HTMLInputElement>document.getElementById("expression");
var output: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("output");
var arrayTemp: string[];

// Tried to implement seperateExpression all most of the cases. Works on all of the input
// But works better if output specified matches the placeholder

function seperateExpression() {
 var expr: string = expression.value;
 var positionI: number = expr.indexOf('i');
 // If no + or - is in the expressio
 if (expr.indexOf('+') == -1 && expr.indexOf('-') == -1) {
  if (positionI == -1) {
   output.innerHTML = "Real Part: " + expr + "<br>Imaginary Part: " + 0;
  } else {
   if (positionI <= expr.length - 2) {
    output.innerHTML = "Real Part: " + 0 + "<br>Imaginary Part: " + expr.substring(positionI + 1, expr.length);
   } else if (positionI >= expr.length - 2 && positionI != 0) {
    output.innerHTML = "Real Part: " + 0 + "<br>Imaginary Part: " + expr.substring(0, positionI);
   } else {
    output.innerHTML = "Real Part: " + 0 + "<br>Imaginary Part: " + 1;
   }
  } 
 } else if (expr.indexOf('+') != -1) { //If expression contais addition
  arrayTemp = expr.split('+');
  if (arrayTemp[0].indexOf('i') != -1) {
   arrayTemp.reverse();
  }
  for(var i = 0; i < arrayTemp.length; i++) {
    arrayTemp[i] = arrayTemp[i].trim();
   }
  positionI = arrayTemp[1].indexOf('i');

  if (positionI == -1) {
   output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: " + 0;
  } else {
   if (positionI <= arrayTemp[1].length - 2) {
    output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: " + arrayTemp[1].substring(positionI + 1, arrayTemp[1].length);
   } else if (positionI >= arrayTemp[1].length - 2 && positionI != 0) {
    output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: " + arrayTemp[1].substring(0, positionI);
   } else {
    output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: " + 1;
   } 
  } // If subtraction is there in the expression
 } else {
  arrayTemp = expr.split("-");
  for(var i = 0; i < arrayTemp.length; i++) {
   arrayTemp[i] = arrayTemp[i].trim();
  }
  if (arrayTemp[0].indexOf('i') != -1) {
   arrayTemp.reverse();
  }
  positionI = arrayTemp[1].indexOf('i');

  console.log(expr.lastIndexOf('-') + " " + expr.indexOf('-'));

  if(expr.lastIndexOf('-') == expr.indexOf('-')) {
   if (positionI == -1) {
    output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: " + 0;
   } else {
    if (positionI <= arrayTemp[1].length - 2) {
     output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: -" + arrayTemp[1].substring(positionI + 1, arrayTemp[1].length);
    } else if (positionI >= arrayTemp[1].length - 2 && positionI != 0) {
     output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: -" + arrayTemp[1].substring(0, positionI);
    } else {
     output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: -" + 1;
    }
  }
  } else {
   expr = expr.replace("i", "");
    output.innerHTML = "Real Part: " + expr.substring(0,expr.lastIndexOf('-')) + "<br>Imaginary Part: -" +expr.substring(expr.lastIndexOf('-') + 1,expr.length);
  }
}
}
