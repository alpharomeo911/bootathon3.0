var expression = document.getElementById("expression");
var output = document.getElementById("output");
var arrayTemp;
function seperateExpression() {
    var expr = expression.value;
    var positionI = expr.indexOf('i');
    if (expr.indexOf('+') == -1 && expr.indexOf('-') == -1) {
        if (positionI == -1) {
            output.innerHTML = "Real Part: " + expr + "<br>Imaginary Part: " + 0;
        }
        else {
            if (positionI <= expr.length - 2) {
                output.innerHTML = "Real Part: " + 0 + "<br>Imaginary Part: " + expr.substring(positionI + 1, expr.length);
            }
            else if (positionI >= expr.length - 2 && positionI != 0) {
                output.innerHTML = "Real Part: " + 0 + "<br>Imaginary Part: " + expr.substring(0, positionI);
            }
            else {
                output.innerHTML = "Real Part: " + 0 + "<br>Imaginary Part: " + 1;
            }
        }
    }
    else if (expr.indexOf('+') != -1) {
        arrayTemp = expr.split('+');
        if (arrayTemp[0].indexOf('i') != -1) {
            arrayTemp.reverse();
        }
        for (var i = 0; i < arrayTemp.length; i++) {
            arrayTemp[i] = arrayTemp[i].trim();
        }
        positionI = arrayTemp[1].indexOf('i');
        if (positionI == -1) {
            output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: " + 0;
        }
        else {
            if (positionI <= arrayTemp[1].length - 2) {
                output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: " + arrayTemp[1].substring(positionI + 1, arrayTemp[1].length);
            }
            else if (positionI >= arrayTemp[1].length - 2 && positionI != 0) {
                output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: " + arrayTemp[1].substring(0, positionI);
            }
            else {
                output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: " + 1;
            }
        }
    }
    else {
        arrayTemp = expr.split("-");
        for (var i = 0; i < arrayTemp.length; i++) {
            arrayTemp[i] = arrayTemp[i].trim();
        }
        if (arrayTemp[0].indexOf('i') != -1) {
            arrayTemp.reverse();
        }
        positionI = arrayTemp[1].indexOf('i');
        console.log(expr.lastIndexOf('-') + " " + expr.indexOf('-'));
        if (expr.lastIndexOf('-') == expr.indexOf('-')) {
            if (positionI == -1) {
                output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: " + 0;
            }
            else {
                if (positionI <= arrayTemp[1].length - 2) {
                    output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: -" + arrayTemp[1].substring(positionI + 1, arrayTemp[1].length);
                }
                else if (positionI >= arrayTemp[1].length - 2 && positionI != 0) {
                    output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: -" + arrayTemp[1].substring(0, positionI);
                }
                else {
                    output.innerHTML = "Real Part: " + arrayTemp[0] + "<br>Imaginary Part: -" + 1;
                }
            }
        }
        else {
            expr = expr.replace("i", "");
            output.innerHTML = "Real Part: " + expr.substring(0, expr.lastIndexOf('-')) + "<br>Imaginary Part: -" + expr.substring(expr.lastIndexOf('-') + 1, expr.length);
        }
    }
}
//# sourceMappingURL=app.js.map