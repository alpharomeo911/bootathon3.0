function check1() {
    var t1 = document.getElementById("num1");
    var t2 = document.getElementById("num2");
    var t3 = document.getElementById("num3");
    var number1 = +t1.value;
    var number2 = +t2.value;
    var number3 = +t3.value;
    if (number1 == number2 && number2 == number3) {
        document.getElementById("t1").innerHTML = "Equilateral Triangle";
    }
    else if (number1 == number2 || number2 == number3) {
        document.getElementById("t1").innerHTML = "Isosceles Triangle";
        if ((Math.pow(number1, 2) == (Math.pow(number2, 2) + Math.pow(number3, 3))) || Math.pow(number2, 2) == (Math.pow(number1, 2) + Math.pow(number3, 3)) || Math.pow(number3, 2) == (Math.pow(number2, 2) + Math.pow(number1, 3))) {
            document.getElementById("t2").innerHTML = "Right Angled Triangle";
        }
    }
    else {
        document.getElementById("t1").innerHTML = "Scalen Triangle";
        if ((Math.pow(number1, 2) == (Math.pow(number2, 2) + Math.pow(number3, 3))) || Math.pow(number2, 2) == (Math.pow(number1, 2) + Math.pow(number3, 3)) || Math.pow(number3, 2) == (Math.pow(number2, 2) + Math.pow(number1, 3))) {
            document.getElementById("t2").innerHTML = "Right Angled Triangle";
        }
    }
}
//# sourceMappingURL=checking.js.map