var input = document.getElementById("input");
function promptUser() {
    var num = 0;
    var possitiveNumber = 0;
    var negativeNumber = 0;
    var zero = 0;
    var loopNumber = parseInt(input.value);
    while (num < loopNumber) {
        var temp = parseInt(prompt("Enter a number: "));
        if (temp > 0) {
            possitiveNumber++;
        }
        else if (temp == 0) {
            zero++;
        }
        else {
            negativeNumber++;
        }
        num++;
    }
    document.getElementById("output").innerHTML = "Positive Number : " + possitiveNumber + "<br>Zero : " + zero + "<br>Negative Number : " + negativeNumber;
}
//# sourceMappingURL=app.js.map