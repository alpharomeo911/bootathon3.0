var input : HTMLInputElement = <HTMLInputElement> document.getElementById("input");

function promptUser() {
 var num : number = 0;
 var possitiveNumber : number = 0;
 var negativeNumber : number = 0;
 var zero : number = 0;
 var loopNumber : number = parseInt(input.value);
 while(num < loopNumber) {
  var temp : number = parseInt(prompt("Enter a number: "));
  if(temp > 0) {
   possitiveNumber++;
  } else if (temp == 0) {
   zero++;
  } else if (temp < 0) {
   negativeNumber++;
  }
  num++;
 }
 document.getElementById("output").innerHTML = "Positive Number : " + possitiveNumber + "<br>Zero : " + zero + "<br>Negative Number : " + negativeNumber;
}