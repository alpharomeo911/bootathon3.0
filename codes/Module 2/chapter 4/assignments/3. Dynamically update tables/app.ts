var tableNumber : HTMLInputElement = <HTMLInputElement> document.getElementById("numberTable");
var upto : HTMLInputElement = <HTMLInputElement> document.getElementById("upto");
var table : HTMLTableElement = <HTMLTableElement> document.getElementById("tableG");

function calculate() {
 while(table.rows.length > 1) {
  table.deleteRow(1);
 }
 
 var numTable : number = parseInt(tableNumber.value);
 var toFindUpto : number = parseInt(upto.value);

 for(var count = 1; count <= toFindUpto; count++) {
  // Creating a row and a cell.
  var row : HTMLTableRowElement = table.insertRow();
  var cell : HTMLTableCellElement = row.insertCell();
  cell.innerHTML =  numTable + " * " + count + " = " + (numTable * count);
 }
}