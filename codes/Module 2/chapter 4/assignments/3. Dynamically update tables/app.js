var tableNumber = document.getElementById("numberTable");
var upto = document.getElementById("upto");
var table = document.getElementById("tableG");
function calculate() {
    while (table.rows.length > 1) {
        table.deleteRow(1);
    }
    var numTable = parseInt(tableNumber.value);
    var toFindUpto = parseInt(upto.value);
    for (var count = 1; count <= toFindUpto; count++) {
        var row = table.insertRow();
        var cell = row.insertCell();
        cell.innerHTML = numTable + " * " + count + " = " + (numTable * count);
        // var text : HTMLParagraphElement = document.createElement("p");
        // text.innerHTML = numTable + " * " + count + " = " + (numTable * count);
        // cell.appendChild(text);
    }
}
//# sourceMappingURL=app.js.map