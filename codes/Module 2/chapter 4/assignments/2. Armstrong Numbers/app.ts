var output : HTMLParagraphElement = <HTMLParagraphElement> document.getElementById("output");

var MIN : number = 100;
var MAX : number = 999;

for(var i = MIN; i <= MAX; i++) {
 var temp : number = i;
 var sum : number = 0;
 while(temp > 0) {
  sum += Math.pow((temp % 10), 3);
  temp = temp / 10;
  temp = Math.floor(temp);
 } 
 if (sum == i) {
  output.innerHTML += i + "<br>";
 }
}