function calculateTrig() {
    var x = document.getElementById("x");
    var conx = +x.value;
    // Since degree and radians was not specified, using radians, if degrees is required, use x * (Math.PI / 180)
    conx += Math.cos(conx);
    conx = Math.round(conx * 100) / 100;
    document.getElementById("output").innerHTML = "<b>x + cos(x) = </b>" + conx;
}
//# sourceMappingURL=trig.js.map