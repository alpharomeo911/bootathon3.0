function areaCircle() {
    var radius = document.getElementById("radius");
    var output = document.getElementById("output");
    var area = Math.PI * Math.pow(parseFloat(radius.value), 2);
    area = Math.round(area * 100) / 100;
    output.innerHTML = "<b>The area of the circle with radius " + radius.value + " is </b>" + area;
}
//# sourceMappingURL=areasc.js.map