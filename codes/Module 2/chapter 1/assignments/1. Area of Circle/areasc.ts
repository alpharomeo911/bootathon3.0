function areaCircle() {
 var radius:HTMLInputElement = <HTMLInputElement>document.getElementById("radius");
 
 var output:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("output");

 var area : number = Math.PI * Math.pow(parseFloat(radius.value), 2);
 
 area = Math.round(area * 100) / 100; 

 output.innerHTML = "<b>The area of the circle with radius " + radius.value + " is </b>" + area;
}