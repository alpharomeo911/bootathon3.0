function triangleArea() {
 // Getting the points in variables
 var a1 : HTMLInputElement = <HTMLInputElement> document.getElementById("point1x");
 var b1 : HTMLInputElement = <HTMLInputElement> document.getElementById("point1y");
 var a2 : HTMLInputElement = <HTMLInputElement> document.getElementById("point2x");
 var b2 : HTMLInputElement = <HTMLInputElement> document.getElementById("point2y");
 var a3 : HTMLInputElement = <HTMLInputElement> document.getElementById("point3x");
 var b3 : HTMLInputElement = <HTMLInputElement> document.getElementById("point3y");

 // Storing the values in different variables

 var x1 : number = parseFloat(a1.value);
 var y1 : number = parseFloat(b1.value);
 var x2 : number = parseFloat(a2.value);
 var y2 : number = parseFloat(b2.value);
 var x3 : number = parseFloat(a3.value);
 var y3 : number = parseFloat(b3.value);

 console.log(x1);

 // Calculating a, b, c
 // Since Math.sqrt returns NAN if the number is negative, hence Math.abs is used.
 var a : number = Math.sqrt(Math.abs(Math.pow((x2 - x1), 2) - Math.pow((y2 - y1), 2)));
 var b : number = Math.sqrt(Math.abs(Math.pow((x3 - x2), 2) - Math.pow((y3 - y2), 2)));
 var c : number = Math.sqrt(Math.abs(Math.pow((x1 - x3), 2) - Math.pow((y1 - y3), 2)));


 // Calculating S

 var s : number;
 s = (a + b + c);
 s = s / 2;

 console.log(s);

 // Calculating the Area of triangle using heron's formula

 var area : number = Math.sqrt(Math.abs((s * (s - a) * (s - b) * (s - c))));

 console.log(area);
 area = Math.round(area * 100) / 100;

 document.getElementById("result").innerHTML = "<strong>Area of trinagle :</strong>" + area;

}