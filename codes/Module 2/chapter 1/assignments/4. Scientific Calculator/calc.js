var number1 = document.getElementById("num1");
var number2 = document.getElementById("num2");
var result = document.getElementById("solution");
var degs = document.getElementById("degree");
// For addition
function addition() {
    if (number1.value == "") {
        number1.value = "0";
    }
    if (number2.value == "") {
        number2.value = "0";
    }
    result.value = (parseFloat(number1.value) + parseFloat(number2.value)).toString();
}
// For subtraction
function subtraction() {
    if (number1.value == "") {
        number1.value = "0";
    }
    if (number2.value == "") {
        number2.value = "0";
    }
    result.value = (parseFloat(number1.value) - parseFloat(number2.value)).toString();
}
// from multiplcation
function multiplication() {
    if (number1.value == "") {
        number1.value = "0";
    }
    if (number2.value == "") {
        number2.value = "0";
    }
    result.value = (parseFloat(number1.value) * parseFloat(number2.value)).toString();
}
// for division
function division() {
    if (number1.value == "") {
        number1.value = "0";
    }
    if (number2.value == "") {
        number2.value = "0";
    }
    result.value = (parseFloat(number1.value) / parseFloat(number2.value)).toString();
}
// Calculates sin of number checks wether degree is required or radians. I could have used condition wether if num1 is empty and num2 is not so calculate sin from num2
// but for simplicity its done like this.
function sine() {
    if (!(number1.value == "")) {
        if (degs.checked) {
            result.value = (Math.sin(parseFloat(number1.value) * (Math.PI / 180))).toString();
        }
        else {
            result.value = (Math.sin(parseFloat(number1.value))).toString();
        }
    }
    else {
        alert("Please Use input 1 for this operation!.");
    }
}
// For Cos
function cosine() {
    if (!(number1.value == "")) {
        if (degs.checked) {
            result.value = (Math.cos(parseFloat(number1.value) * (Math.PI / 180))).toString();
        }
        else {
            result.value = (Math.cos(parseFloat(number1.value))).toString();
        }
    }
    else {
        alert("Please Use input 1 for this operation!.");
    }
}
// for tan
function tangent() {
    if (!(number1.value == "")) {
        if (degs.checked) {
            result.value = (Math.tan(parseFloat(number1.value) * (Math.PI / 180))).toString();
        }
        else {
            result.value = (Math.tan(parseFloat(number1.value))).toString();
        }
    }
    else {
        alert("Please Use input 1 for this operation!.");
    }
}
// For squareRoot
function squareRoot() {
    if (!(number1.value == "")) {
        result.value = (Math.sqrt(parseFloat(number1.value))).toString();
    }
    else {
        alert("Please Use input 1 for this operation!.");
    }
}
// for power
function power() {
    if (number1.value == "") {
        number1.value = "0";
    }
    if (number2.value == "") {
        number2.value = "0";
    }
    result.value = (Math.pow(parseFloat(number1.value), parseFloat(number2.value))).toString();
}
// For clearing the field
function clearField() {
    number1.value = "";
    number2.value = "";
    result.value = "";
}
//# sourceMappingURL=calc.js.map